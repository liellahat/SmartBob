import tensorflow as tf
import numpy as np
import data_parser as dp
from tqdm import tqdm
import re


class Model:

	def __init__(self, device = 'gpu', data_path = 'data/', checkpoint_path = 'models/model1/model.ckpt', hidden_size = 256, embed_size = 256, batch_size = 32, scope = 'model'):	
		self.checkpoint_path = checkpoint_path
		self.data_path = data_path
		self.device = device
		self.scope = scope
		self.hidden_size = hidden_size
		self.input_length = 21
		self.embed_size = embed_size
		self.batch_size = batch_size
		
		if self.device.lower() == 'cpu':
			config = tf.ConfigProto(device_count = {'GPU': 0})			
			self.sess = tf.Session(config = config)
		elif self.device.lower() == 'gpu':
			self.sess = tf.Session()
		
		self.__gather_data()
		self.__build_model()
		print('Model', self.scope, 'built')		
		tf.global_variables_initializer().run(session=self.sess)
		print('Variables initialized.')
		
	    # loading checkpoint
		self.saver = tf.train.Saver(tf.global_variables())
		try:  
			self.saver.restore(self.sess, self.checkpoint_path)  # try to load weights if there are any.
		except:
			print('Valid checkpoint file not found at ' + self.checkpoint_path + '. Starting from scratch.')
		
	
	def __gather_data(self):
		data, self.int_to_word, self.word_to_int = dp.parse_data(self.data_path)
			
		input_data = []
		output_data = []
		
		for pair in data:
			curr = []
			for word in pair[0]:
				curr.append(self.word_to_int[word])
			
			padding_val = self.input_length - len(pair[0])
			for i in range(padding_val):
				curr = [self.word_to_int['<PAD>']] + curr
				
			input_data.append(curr)
			
			curr = [self.word_to_int['<START>']]
			for word in pair[1]:
				curr.append(self.word_to_int[word])
				
			padding_val = self.input_length - len(pair[1]) 
			for i in range(padding_val):
				curr.append(self.word_to_int['<PAD>'])
			
			output_data.append(curr)
			
			
		self.data_size = len(input_data)
		self.vocab_size = len(self.word_to_int)
		print('Data size: ' + str(self.data_size))
		print('Vocab size: ' + str(self.vocab_size))
		self.dataX = np.asarray(input_data)
		self.dataY = np.asarray(output_data)
		
	
	def __build_model(self):		
		with tf.variable_scope(self.scope + '-placeholders'):		
			self.inputs = tf.placeholder(tf.int32,[None, self.input_length], name='inputs')
			self.outputs = tf.placeholder(tf.int32, [None, None], name='outputs')
			self.targets = tf.placeholder(tf.int32, [None, None], name='targets')	
		
		with tf.variable_scope(self.scope + 'embedding'):
			input_embedding = tf.Variable(tf.random_uniform((self.vocab_size, self.embed_size), -1.0, 1.0, seed = 1))
			output_embedding = tf.Variable(tf.random_uniform((self.vocab_size, self.embed_size), -1.0, 1.0, seed = 1))
			input_embed = tf.nn.embedding_lookup(input_embedding, self.inputs)
			output_embed = tf.nn.embedding_lookup(output_embedding, self.outputs)
		
		with tf.variable_scope(self.scope + '-encoder'):
			lstm_enc_1 = tf.contrib.rnn.LSTMCell(self.hidden_size, reuse=tf.AUTO_REUSE)
			lstm_enc_2 = tf.contrib.rnn.LSTMCell(self.hidden_size, reuse=tf.AUTO_REUSE)
			_, last_state = tf.nn.dynamic_rnn(tf.contrib.rnn.MultiRNNCell(cells=[lstm_enc_1, lstm_enc_2]), inputs=input_embed, dtype=tf.float32)
			
		with tf.variable_scope(self.scope + '-decoder'):
			lstm_dec_1 = tf.contrib.rnn.LSTMCell(self.hidden_size, reuse=tf.AUTO_REUSE) 
			lstm_dec_2 = tf.contrib.rnn.LSTMCell(self.hidden_size, reuse=tf.AUTO_REUSE) 
			dec_outputs, _ = tf.nn.dynamic_rnn(tf.contrib.rnn.MultiRNNCell(cells=[lstm_dec_1, lstm_dec_2]), inputs=output_embed, initial_state=last_state, dtype=tf.float32)			

			self.logits = tf.layers.dense(dec_outputs, units=self.vocab_size, use_bias=True) 
			
		with tf.variable_scope(self.scope + '-optimizing'):
			self.loss = tf.contrib.seq2seq.sequence_loss(self.logits, self.targets, tf.ones([self.batch_size, self.input_length]))
			
			self.optimizer = tf.train.RMSPropOptimizer(0.001).minimize(self.loss)
			
			
	def train(self, epoches):
		n_batches = int(self.data_size / self.batch_size)
		min_loss = float('inf')			
		s = np.arange(self.dataX.shape[0])
		
		for k in range(epoches):
			print('Epoch ' + str(k) + ':')
			np.random.shuffle(s)
			self.dataX = self.dataX[s]
			self.dataY = self.dataY[s]
			avg = 0
			count = 0
			i = 0
			pbar = tqdm(range(n_batches), ncols = 100)
			for k in pbar:
				in_batch = self.dataX[count : count + self.batch_size]
				out_batch = self.dataY[count : count + self.batch_size]
				
				_, loss = self.sess.run([self.optimizer, self.loss], feed_dict = {self.inputs: in_batch, self.outputs: out_batch[:, :-1], self.targets: out_batch[:, 1:]})
				avg = ((avg * i + loss) / (i + 1))
				
				pbar.set_description('Pattern: {} Avg loss: {:.4f}'.format(count, avg))
				pbar.refresh()
				count += self.batch_size
				i += 1
			
			if avg < min_loss:
				print('Loss improved from', min_loss, 'to', avg)
				min_loss = avg
				self.saver.save(self.sess, self.checkpoint_path)
			
	
	def talk(self, string):
		input = []
		for i in string.split(' '):
			input.append(self.word_to_int[i])
	
		padding_val = self.input_length - len(input)
		for _ in range(padding_val):
			input = [self.word_to_int['<PAD>']] + input			
		
		output = ''
		dec_input = [self.word_to_int['<START>']]
		
		for _ in range(self.input_length):
			prediction = self.sess.run(self.logits, feed_dict = {self.inputs: [input], self.outputs: [dec_input]})
			index = np.argmax(prediction[: , -1])
			if self.int_to_word[index] == '<PAD>':
				break
			dec_input.append(index)
			output += (self.int_to_word[index] + ' ')
			
		return output			