import os

letters = 'abcdefghijklmnopqrstuvwxyz'
numbers = '1234567890'
special = '.,!?()-/\n\t:;"\''

def str_to_words(s):
	list = []
	
	curr_word = ''
	for i in range(len(s)):
		if s[i] in letters or s[i] in numbers:
			curr_word += s[i]
			
		elif s[i] == ' ':
			if curr_word != '':
				list.append(curr_word)
			curr_word = ''
			
		elif s[i] == '\'' \
		and i != 0 and i < len(s) - 1 \
		and s[i + 1] in letters and (s[i - 1] in letters or s[i - 1] in numbers):  # if its a mid-word or mid_number ' (it's, 70's) do nothing
			pass
			
		elif s[i] in special:
			if curr_word != '':
				list.append(curr_word)
			list.append(s[i])
			curr_word = ''
	if curr_word != '':
		list.append(curr_word)
	return list
			

def parse_data(dirpath):
	data = ''
	for filename in os.listdir(dirpath):
		f = open(dirpath + filename, 'r')
		data += f.read()
		f.close()
		
	pairs = [[pair.split('&')[0], pair.split('&')[1]] for pair in data.split('@')[:-1]]
	
	counter = {}
	parsed_data = []
	
	for index, pair in enumerate(pairs):	
		x = str_to_words(pair[0])
		y = str_to_words(pair[1])
		parsed_data.append([x, y])
		
		for i in x + y:
			if i in counter:
				counter[i][0] += 1
			else:
				counter[i] = [1, index]
				
				
	words = set()
	deletions = 0
	
	for w in counter.keys():
		index = counter[w][1]
		if counter[w][0] > 1:
			words.add(w)
		else:
			print(w)
			del parsed_data[index - deletions]
			deletions += 1
	
	int_to_word = {n : w for n, w in enumerate(words)}
	int_to_word[len(words)] = '<PAD>'
	int_to_word[len(words) + 1] = '<START>'
	
	word_to_int = {w : n for n, w in int_to_word.items()}
	
	return parsed_data, int_to_word, word_to_int
