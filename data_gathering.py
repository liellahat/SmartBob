import praw
import re
import os
import data_parser


allowed_chars = 'abcdefghijklmnopqrstuvwxyz1234567890.,!?()- /\n\t:;"\''
unallowed = ['[removed]', '[deleted]', '/r/', '/n/', 'n/', 'r/', '/u/', 'u/']
replaces = { '’' : '\'', '`' : '\'',
			 '”' : '"', '“' : '"' }
# liel
subreddit_names = ['memes', 'dankmemes', 'madlads']
#ori
"""subreddit_names=				   ['dank_meme', 'me_irl', 'see', 'memeeconomy', 'cringepics', 'instant_regret',
				   'blunderyears', 'cringeanarchy', 'sadcringe', 'niceguys', 'nocontext', 'holdmybeer', 'holdmyjuicebox', 'holdmyfries',
				   'holdmybeaker', 'holdmyredbull', 'fiftyfifty', 'firstworldproblems', 'idiotsfightingthings', 'notinteresting', 'whatsinthisthing',
				   'notmyjob', 'hmmm']
"""

def change_chars(s):
	s2 = ''
	for char in s:
		if char in replaces.keys():
			char = replaces[char]
		s2 += char	
		
	return s2
	

def is_valid(s):
	if len(data_parser.str_to_words(s)) > 21:
		return False
	
	for word in s.split(' '):
		if len(word) > 21:
			return False
		
	for char in s:
		if char not in allowed_chars:
			return False
			
	for i in unallowed:
		if i in s:
			return False

	if len(re.findall('https?://(?:[-\w.]|(?:%[\da-fA-F]{2}))+', s)) > 0:
		return False
		
	return True
			

def scan(comment, f):
	if type(comment).__name__ != "MoreComments" and len(comment.replies) > 0:
		c = change_chars(comment.body.lower())
		if is_valid(c):
			for reply in comment.replies:
				if type(reply).__name__ != "MoreComments":					
					r = change_chars(reply.body.lower())
					pair = ''
					if is_valid(r):
						pair = c + '&' + r + '@'
						print(pair)
						f.write(pair)
						scan(reply, f)
		else:
			for reply in comment.replies:
				if type(reply).__name__ != "MoreComments":
					scan(reply, f)
		
			
reddit = praw.Reddit(client_id = 'tiUA7YC_oTJtlA', 
					 client_secret = 'rvsom6lNtaiLu86PC5AOhI4tBuU',
					 username = 'not_thresh',
					 password = 'bob12345654321',
					 user_agent = 'nothing_important')

for subreddit_name in subreddit_names:
	hot = reddit.subreddit(subreddit_name).hot(limit = 999999999999)
	f = open(subreddit_name + '.txt', 'w')
	for i in hot:
		comments = i.comments
		for comment in comments:
			scan(comment, f)
		
	f.close()
		